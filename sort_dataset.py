##### Bunch of small function to sort dataset (RoboSoft2023) #####

import os
# from mrcnn.dataset import FoodChallengeDataset
import csv
import pandas as pd
import json
import ast
import cv2
import numpy as np
import shutil
import json
from sklearn.model_selection import train_test_split
# dataset_train = FoodChallengeDataset()
# dataset_train.load_dataset(dataset_dir=os.path.join("/home/jessica/Desktop/softrobot_competition/FoodDataset/FoodRecognitionBenchmark2022", "public_training_set_release_2.1"), load_small=False)
# dataset_train.prepare()
# dataset = dataset_train
# print("Image Count: {}".format(len(dataset_train.image_ids)))
# print("Class Count: {}".format(dataset_train.num_classes))
# class_id_name=[]
class_file='classinfo_public_training_set_release_2.1.csv'
class_needed = ['sausage' , 'ball' , 'broccoli' , 'carrot' , 'bean' , 'spaghetti' , 'cookie' , 'egg' , 'juice']
annotation_file='food_annotation_public_training_set_release_2.0.csv'
def sort_class_info(file):
    with open(file, 'w', encoding='UTF8', newline='') as f:
        fieldnames=['source', 'id', 'name']
        writer = csv.DictWriter(f, fieldnames=fieldnames)
        writer.writeheader()
    for i, info in enumerate(dataset_train.class_info):
        # print("{:3}. {:50}".format(i, info['name']))
        if any([x in info['name'] for x in class_needed]):
            # class_id_name.append([info['id'],info['name']])
            print('name:',info['name'],' class_id:',info['id'])

            with open(file, 'a', encoding='UTF8', newline='') as f:
                writer = csv.DictWriter(f, fieldnames=fieldnames)
                writer.writerow(info)

def get_class_info(file):
    df = pd.read_csv(file)
    # print(df)
    # print(len(df['id']))

    return df

def get_image_need(classinfo,file):
    class_id=classinfo['id'].tolist()
    dict = {}
    num_image=0

    for n in range(len(class_id)):
        category_id = str(class_id[n])
        dict[category_id]=0
    print(dict)
    # with open(file, 'w', encoding='UTF8', newline='') as f:
    #     fieldnames=['id', 'source', 'path','width','height','annotations']
    #     writer = csv.DictWriter(f, fieldnames=fieldnames)
    #     writer.writeheader()

    for i, img_info in enumerate(dataset_train.image_info):
        num_image+=1
        # count = 0
        # print(img_info['annotations'])
        flag=False
        new_img_info=img_info.copy()
        new_img_info['annotations']=[]
        for j, info in enumerate(img_info['annotations']):
            # print(info['category_id'])
            # print(j)
            if int(info['category_id']) in class_id:
                new_img_info['annotations'].append(info)
                category_id=str(info['category_id'] )
                dict[category_id]+=1
                flag=True
                # count+=1
        # if count>1:
        #     print(new_img_info)
        # if flag:
        #     with open(file, 'a', encoding='UTF8', newline='') as f:
        #         writer = csv.DictWriter(f, fieldnames=fieldnames)
        #         writer.writerow(new_img_info)
    print(dict)
    print('num_image:',num_image)

def sort_image(data,classinfo):
    for j, info in enumerate(data['annotations']):
        origin=data['path'][j]
        target=os.path.join('/home/jessica/Desktop/softrobot_competition/dataset/FoodRecognitionBenchmark2022/public_training_set_release_2.1/images',str(data['id'][j])+'.jpg')
        print('origin:',origin)
        print('target:',target)
        # shutil.copy(origin, target)
        objects = ast.literal_eval(info)
        for object in objects:
            print(object['category_id'])
            catagory=classinfo['name'][classinfo['id']==object['category_id']]
            catagory=catagory.values[0]
            # Check whether the specified path exists or not
            folder=os.path.join('/home/jessica/Desktop/softrobot_competition/dataset/FoodRecognitionBenchmark2022/public_training_set_release_2.1',catagory)
            target_catagory=os.path.join(folder,str(data['id'][j])+'.jpg')
            isExist = os.path.exists(folder)
            # if not isExist:
                # Create a new directory because it does not exist
                # os.makedirs(folder)
            print('target catagory:', target_catagory)
            # shutil.copy(origin, target_catagory)

def change_image_size():
    folderpath = os.path.join("/home/jessica/Desktop/softrobot_competition/dataset/Own", "box2")
    dir = os.listdir(folderpath)
    for i in range(len(dir)):
        image_path=os.path.join(folderpath, dir[i])
        print('image_path:',image_path)
        img = cv2.imread(image_path)
        scale_percent = 3
        width = int(img.shape[1] / scale_percent)
        height = int(img.shape[0] / scale_percent)
        dim = (width, height)
        resized = cv2.resize(img, dim, interpolation=cv2.INTER_AREA)
        folderpath_new=os.path.join("/home/jessica/Desktop/softrobot_competition/dataset/Own", "box2s")
        filename=os.path.join(folderpath_new, dir[i])
        print('filename:',filename)
#         cv2.imwrite(filename, resized)
# change_image_size()

def get_annotation_of_catagery_wanted(data,file):
    catagery_wanted=[1924,2454,2376,843,2413,1831,1883,1856,1908]
    sausage_id=[1924,843,1831,1883,1856,1908]
    cookies_id=[2376,2413]
    # with open(file, 'w', encoding='UTF8', newline='') as f:
    #     fieldnames=['id', 'source','filename','width','height','annotations']
    #     writer = csv.DictWriter(f, fieldnames=fieldnames)
    #     writer.writeheader()
    i=2120
    for j, info in enumerate(data['annotations']):
        # new_info=[]
        new_dict={}
        new_anno = []
        flag=False
        objects = ast.literal_eval(info)
        for object in objects:
            if int(object['category_id']) in catagery_wanted:
                # print(object['category_id'])
                if int(object['category_id'])==2454: #orange juice
                    object['category_id']=10
                if int(object['category_id']) in sausage_id:
                    object['category_id'] = 14
                if int(object['category_id']) in cookies_id:
                    object['category_id'] = 15
                new_anno.append(object)
                object['id']=i
                i+=1
                # if int(object['id'])<=2119:
                #     print('Tere have object id less than 2119')
                #     print(object['id'])
                #     print(object['image_id'])
                # if int(object['image_id'])<=399:
                #     print('Tere have image id less than 399')
                #     print(object['id'])
                #     print(object['image_id'])
                flag = True
        if flag:
            new_dict['id']=data['id'][j]
            new_dict['source'] = data['source'][j]
            new_dict['filename'] = os.path.basename(data['path'][j])
            new_dict['width'] = data['width'][j]
            new_dict['height'] = data['height'][j]
            new_dict['annotations'] = new_anno
            # new_info.append(data['id'][j])
            # new_info.append(data['source'][j])
            # new_info.append(data['path'][j])
            # new_info.append(data['width'][j])
            # new_info.append(data['height'][j])
            # new_info.append(new_anno)
            # print(new_dict)
            # with open(file, 'a', encoding='UTF8', newline='') as f:
            #     writer = csv.DictWriter(f, fieldnames=fieldnames)
            #     writer.writerow(new_dict)

def get_category_want_json():
    json_path = "/home/jessica/Desktop/softrobot_competition/FoodDataset/FoodRecognitionBenchmark2022/public_training_set_release_2.1/annotation.json"
    category_image_foler='/home/jessica/Desktop/softrobot_competition/dataset/FoodRecognitionBenchmark2022/public_training_set_release_2.1/cookies'
    save_json_path='/home/jessica/Desktop/softrobot_competition/dataset/new_RoboSoft2_FoodRecognitionBenchmark2022/_annotations.coco.json'
    category_id_wanted=[100725]
    f = open(json_path)
    # returns JSON object as
    # a dictionary
    data = json.load(f)
    new_data={}
    new_data['images']=[]
    new_data['annotations'] = []
    dir = os.listdir(category_image_foler)
    for i in range(len(dir)):
        image_name=dir[i]
        for _,image in enumerate(data['images']):
            if int(image['file_name'].split('.')[0])==int(image_name.split('.')[0]):
                new_data['images'].append(image)
    for _, anno in enumerate(data['annotations']):
        # print(anno['category_id'])
        if int(anno['category_id']) in category_id_wanted:
            new_data['annotations'].append(anno)

    # with open(save_json_path, 'w') as outfile:
    #     json.dump(new_data, outfile)

# get_category_want_json()



def image(row):
    image = {}
    image["id"] = row.id
    image["file_name"] = row.filename
    image["height"] = row.height
    image["width"] = row.width
    return image

def category(row):
    category = {}
    category["supercategory"] = 'None'
    category["id"] = row.categoryid
    category["name"] = row[2]
    return category

def annotation(object):
    annotation = {}
    # area = (row.xmax - row.xmin) * (row.ymax - row.ymin)
    annotation["id"]=object['id']
    annotation["image_id"] = object['image_id']
    annotation["category_id"] = object['category_id']
    annotation["bbox"] = object['bbox']
    annotation["segmentation"] = object['segmentation']
    annotation["area"] = object['area']
    annotation["iscrowd"] = 0

    return annotation


def csvtococojson():
    path = 'food_annotation_public_training_set_release_2.0_sausage_cookies_orangejuice.csv' # the path to the CSV file
    save_json_path = 'food_annotation_public_training_set_release_2.0_sausage_cookies_orangejuice.json'

    data = pd.read_csv(path)

    images = []
    categories = []
    annotations = []

    categories_name=['orangejuice','sausage','cookies']
    categories_id=['10','14','15']
    for i in range(len(categories_name)):
        category = {}
        category["id"] = categories_id[i]
        category["name"] = categories_name[i]
        category["supercategory"] = 'food-and-tableware'
        categories.append(category)

    for row in data.itertuples():
        objects = ast.literal_eval(row.annotations)
        for object in objects:
            annotations.append(annotation(object))

    for row in data.itertuples():
        images.append(image(row))

    data_coco = {}
    data_coco["categories"] = categories
    data_coco["images"] = images
    data_coco["annotations"] = annotations
    # json.dump(data_coco, open(save_json_path, "w"), indent=4)
def readjson():
    path = 'food_annotation_public_training_set_release_2.0_sausage_cookies_orangejuice_validation.json'
    # Opening JSON file
    f = open(path)

    # returns JSON object as
    # a dictionary
    data = json.load(f)
    print(len(data["categories"]))
    print(len(data["images"]))
    print(len(data["annotations"]))

def change_strtoint():
    path = "/home/jessica/Desktop/softrobot_competition/dataset/RoboSoft Competition 2023.v3i.coco-segmentation/valid/annotations/annotations.coco.json"
    save_json_path="/home/jessica/Desktop/softrobot_competition/dataset/RoboSoft Competition 2023.v3i.coco-segmentation/valid/annotations/_annotations.coco.json"
    # Opening JSON file
    f = open(path)
    # returns JSON object as
    # a dictionary
    data = json.load(f)
    for j, image in enumerate(data["images"]):
        image['height']=int(image['height'])
        image['width'] = int(image['width'])
        image['id'] = int(image['id'])
    for j, anno in enumerate(data["annotations"]):
        anno['id']=int(anno['id'])
        anno['image_id'] = int(anno['image_id'])
        anno['iscrowd'] = int(anno['iscrowd'])
        anno['bbox']=np.float32(anno['bbox']).tolist()
        for i in range(len(anno['segmentation'])):
            anno['segmentation'][i] = np.float32(anno['segmentation'][i]).tolist()
        anno['area'] = float(anno['area'])
    # with open(save_json_path, 'w') as outfile:
    #     json.dump(data, outfile)
# change_strtoint()

def change_imgfile():
    path = "/home/jessica/ros2_ws/src/object_detection/object_detection/surprise_food/FreeSOLO/datasets/coco/annotations/instances_train2017_unlabeled2017_densecl_r101.json"
    save_json_path="/home/jessica/ros2_ws/src/object_detection/object_detection/surprise_food/FreeSOLO/datasets/coco/annotations/instances_train2017_unlabeled2017_densecl_r101.json"
    # Opening JSON file
    f = open(path)
    # returns JSON object as
    # a dictionary
    data = json.load(f)
    for j, image in enumerate(data["images"]):
        image['file_name']=os.path.join('train',image['file_name'])
#     with open(save_json_path, 'w') as outfile:
#         json.dump(data, outfile)
# change_imgfile()

def check_width_height():
    save_json_path = "/home/jessica/Desktop/softrobot_competition/dataset/RoboSoft2_FoodRecognitionBenchmark2022/valid/annotations/_annotations.coco.json"
    f = open(save_json_path)
    # returns JSON object as
    # a dictionary
    data = json.load(f)
    for j, image in enumerate(data["images"]):
        image_path=os.path.join("/home/jessica/Desktop/softrobot_competition/dataset/RoboSoft2_FoodRecognitionBenchmark2022/valid/images",image['file_name'])
        im = cv2.imread(image_path)
        h, w, c = im.shape
        image['height']=h
        image['width'] =w
    # with open(save_json_path, 'w') as outfile:
    #     json.dump(data, outfile)
# check_width_height()


def split_train_test():

    json_path='/home/jessica/Desktop/softrobot_competition/dataset/new_RoboSoft2_FoodRecognitionBenchmark2022/_annotations.coco.json'
    seperate_train_path='/home/jessica/Desktop/softrobot_competition/dataset/new_RoboSoft2_FoodRecognitionBenchmark2022/train/annotations/split.coco.json'
    seperate_validation_path='/home/jessica/Desktop/softrobot_competition/dataset/new_RoboSoft2_FoodRecognitionBenchmark2022/valid/annotations/split.coco.json'
    with open(json_path) as json_file:
        data = json.load(json_file)
    train, test = train_test_split(data['images'], test_size=0.2)
    # Using a JSON string
    # with open(seperate_train_path, 'w') as outfile:
    #     json.dump(train, outfile)
    # with open(seperate_validation_path, 'w') as outfile:
    #     json.dump(test, outfile)

# split_train_test()

def sort_split_train_test():
    json_path = '/home/jessica/Desktop/softrobot_competition/dataset/new_RoboSoft2_FoodRecognitionBenchmark2022/_annotations.coco.json'
    f = open(json_path)
    # returns JSON object as
    # a dictionary
    data = json.load(f)

    save_json_path ='/home/jessica/Desktop/softrobot_competition/dataset/new_RoboSoft2_FoodRecognitionBenchmark2022/valid/annotations/_annotations.coco.json'
    # train_path = 'food_annotation_public_training_set_release_2.0_sausage_cookies_orangejuice_validation.json'
    # seperate_path = 'train.json'
    seperate_path ='/home/jessica/Desktop/softrobot_competition/dataset/new_RoboSoft2_FoodRecognitionBenchmark2022/valid/annotations/split.coco.json'
    with open(seperate_path) as json_file:
        train = json.load(json_file)

    images = []
    image_ids=[]
    annotations = []
    for j, info in enumerate(train):
        images.append(info)
        image_ids.append(info["id"])
        origin = os.path.join('/home/jessica/Desktop/softrobot_competition/FoodDataset/FoodRecognitionBenchmark2022/public_training_set_release_2.1/images',info["file_name"])
        target = os.path.join('/home/jessica/Desktop/softrobot_competition/dataset/new_RoboSoft2_FoodRecognitionBenchmark2022/valid/images',info["file_name"])
        print('origin:', origin)
        print('target:', target)
        # shutil.copy(origin, target)
    for i, anno in enumerate(data["annotations"]):
        if anno["image_id"] in image_ids:
            annotations.append(anno)


    data_coco = {}
    # data_coco["categories"] = data["categories"]
    data_coco["images"] = images
    data_coco["annotations"] = annotations
    # json.dump(data_coco, open(save_json_path, "w"))
# sort_split_train_test()

def check_id():
    json_path = "/home/jessica/Desktop/softrobot_competition/dataset/new_RoboSoft2_FoodRecognitionBenchmark2022/valid/annotations/_annotations.coco.json"
    f = open(json_path)
    # returns JSON object as
    # a dictionary
    data = json.load(f)

    for i, image in enumerate(data["images"]):
        if image['id']<450:
            print('there is image id smaller than 450')
            print(image['file_name'])
    for j, anno in enumerate(data["annotations"]):
        # anno['id']=j+2643
        # print(anno['id'])
        assert anno['category_id']==100725, f"category if not equal to 100725, got: {anno['category_id']}"
        anno['category_id']=16

    # with open(json_path, 'w') as outfile:
    #     json.dump(data, outfile)

# check_id()

def change_annotation():
    onebyone_path = "/home/jessica/Desktop/softrobot_competition/dataset/onebyone.v1i.coco-segmentation/valid/annotations/_annotations_bunch_greenbeans_bottle2_icegems.coco.json"
    new_path = "/home/jessica/Desktop/softrobot_competition/dataset/onebyone.v1i.coco-segmentation/valid/annotations/_annotations_bunch_greenbeans_bottle2_1by1andbunchicegems.coco.json"
    bunch_path="/home/jessica/Desktop/softrobot_competition/dataset/onebyone.v2i.coco-segmentation/valid/annotations/_annotations_bunch_greenbeans_bottle2_bunchicegems.coco.json"
    onebyone_f = open(onebyone_path)
    bunch_f=open(bunch_path)
    # returns JSON object as a dictionary
    data_onebyone = json.load(onebyone_f)
    data_bunch=json.load(bunch_f)

    imageid_onebyone=[]
    imageid_bunch=[]

    data_new=data_onebyone.copy()
    

    for i, onebyone_image in enumerate(data_onebyone["images"]):
        imageid_onebyone.append(onebyone_image['id'])
        for j, bunch_image in enumerate(data_bunch["images"]):
            if onebyone_image['file_name'].split('_')[1]==bunch_image['file_name'].split('_')[1]:
                # print('onebyone_image[file_name]:',onebyone_image['file_name'])
                # print('bunch_image[file_name]:',bunch_image['file_name'])
                imageid_bunch.append(bunch_image['id'])


    # ## remove for 1by1andbunch
    # data_new['annotations']=[]
    # for i, onebyone_anno in enumerate(data_onebyone["annotations"]):
    #     if onebyone_anno['category_id']!=10:
    #         data_new['annotations'].append(onebyone_anno)
    
    for i, bunch_anno in enumerate(data_bunch["annotations"]):
        if bunch_anno['category_id']==17:
            bunch_anno['image_id']= imageid_onebyone[imageid_bunch.index(bunch_anno['image_id'])]
            data_new['annotations'].append(bunch_anno)
    
    for i, new_anno in enumerate(data_new["annotations"]):
        data_new["annotations"][i]['id']=i

#     with open(new_path, 'w') as outfile:
#         json.dump(data_new, outfile)

# change_annotation()

def add_annotation():
    original_path='/home/jessica/Desktop/softrobot_competition/dataset/onebyone.v1i.coco-segmentation/valid/annotations/_annotations_bunch_greenbeans.coco.json'
    add_path='/home/jessica/Desktop/softrobot_competition/dataset/bottle2_1by1icegems.v3i.coco-segmentation/valid/_annotations.coco.json'
    new_path='/home/jessica/Desktop/softrobot_competition/dataset/onebyone.v1i.coco-segmentation/valid/annotations/_annotations_bunch_greenbeans_bottle2_1by1icegems.coco.json'

    original_f = open(original_path)
    add_f=open(add_path)
    # returns JSON object as a dictionary
    data_original = json.load(original_f)
    data_add=json.load(add_f)

    data_new=data_original.copy()

    categoryid_add=[]
    categoryid_add_name=[]
    categoryid_original=[]
    categoryid_original_name=[]

    for i, add_category in enumerate(data_add["categories"]):
        if add_category['name']=='object':
            continue
        if add_category['name']=='icegems':
            categoryid_add.append(add_category['id'])
            categoryid_original.append(17)
            categoryid_add_name.append(add_category['name'])
            categoryid_original_name.append('icegems')
            continue
        categoryid_add.append(add_category['id'])
        categoryid_add_name.append(add_category['name'])
        for j, original_category in enumerate(data_original["categories"]):
            if add_category['name']==original_category['name']:
                categoryid_original.append(original_category['id'])
                categoryid_original_name.append(original_category['name'])
                break
    print('categoryid_add:',categoryid_add)
    print('categoryid_add:',categoryid_add_name)
    print('categoryid_add:',categoryid_original)
    print('categoryid_original:',categoryid_original_name)
    assert len(categoryid_add) == len(categoryid_original), "length of add category id not equal to corresponding length of original category id. STH WRONG!! " 

    total_image=len(data_original['images'])
    total_anno=len(data_original['annotations'])

    for i, add_img in enumerate(data_add["images"]):
        add_img['id']=add_img['id']+total_image
        data_new['images'].append(add_img)

    for i, add_anno in enumerate(data_add["annotations"]):
        add_anno['id']=add_anno['id']+total_anno
        add_anno['image_id']=add_anno['image_id']+total_image
        add_anno['category_id']=categoryid_original[categoryid_add.index(add_anno['category_id'])]
        # print(add_anno['category_id'])
        data_new['annotations'].append(add_anno)


#     with open(new_path, 'w') as outfile:
#         json.dump(data_new, outfile)

# add_annotation()
           

def count_categoty():
    path = "/home/jessica/Desktop/softrobot_competition/dataset/onebyone.v2i.coco-segmentation/train/annotations/_annotations_bunch_greenbeans_box2_bottle2_bunchicegems.coco.json"
    f = open(path)
    data= json.load(f)
    amount_of_object={}
    for i, anno in enumerate(data["annotations"]):
        try:
            amount_of_object[anno['category_id']]+=1
        except:
            amount_of_object[anno['category_id']]=1

    print(amount_of_object)

def check_bb():
    image_path='/home/jessica/Desktop/softrobot_competition/dataset/onebyone.v2i.coco-segmentation/train/images/IMG_1143_JPG_jpg.rf.5213dc223d8973ee545711ebefadb8d2.jpg'
    bb=np.array([668, 786, 108.182, 118.181]).astype(int) 
    image = cv2.imread(image_path)
    cv2.rectangle(image, (bb[0], bb[1]), (bb[0] + bb[2], bb[1] + bb[3]), (0, 255, 0), 2)
    cv2.imshow('image', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
# check_bb()


# sort_split_train_test()
# readjson()
# split_train_test()


# readjson()
# csvtococojson()


# data=get_class_info(annotation_file)
# annotation_specific_food='food_annotation_public_training_set_release_2.0_sausage_cookies_orangejuice.csv'
# get_annotation_of_catagery_wanted(data,annotation_specific_food)

# classinfo=get_class_info(class_file)
# # get_image_need(classinfo,annotation_file)
# sort_image(data,classinfo)
# classinfo=get_class_info(class_file)
# get_image_need(classinfo,annotation_file)