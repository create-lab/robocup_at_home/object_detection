import cv2
import argparse
from detectron2.data.datasets import register_coco_instances
from detectron2.data import DatasetCatalog
from detectron2.data import MetadataCatalog
from detectron2.utils.visualizer import Visualizer

def get_args():
    parser = argparse.ArgumentParser(description='Train detectron2 model')
    parser.add_argument('--image-path', default='', type=str,
                        help='path to image')
    parser.add_argument('--annotation-path', default='', type=str,
                        help='path to annotation')
    args = parser.parse_args()

    return args

if __name__ == '__main__':
    args = get_args()
    
    register_coco_instances('dataset', {},args.annotation_path,args.image_path)
    
    metadata = MetadataCatalog.get('dataset')
    dataset_dicts = DatasetCatalog.get('dataset')

    for d in dataset_dicts:
        img = cv2.imread(d["file_name"])
        visualizer = Visualizer(img, metadata=metadata, scale=0.5)
        vis = visualizer.draw_dataset_dict(d)
        cv2.imshow('image',vis.get_image())
        if cv2.waitKey(0) == ord('q'):
                cv2.destroyAllWindows()
                break
    