from ultralytics import YOLO
import torch
import argparse

def get_args():
    parser = argparse.ArgumentParser(description='Train yolov8 model')
    parser.add_argument('--cfg', default='yolov8_custom_cfg.yaml', type=str,
                        help='path to config file for setting hyperparameters')
    parser.add_argument('--pretrained-model', default='yolov8n.pt', type=str,
                        help='path to the pretrained model')
    parser.add_argument('--image-size', default=512, type=int,
                        help='image size of input image')
    args = parser.parse_args()

    return args  
if __name__ == '__main__':
    args = get_args()
    device=torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    print(device)
    # Load a model
    model = YOLO(model=args.pretrained_model,task='detect')  # load a pretrained model (recommended for training)
    # Train the model
    model.train(imgsz=args.image_size,cfg=args.cfg)
    # model.train(data='yolo_data.yaml', epochs=100, patience=20,imgsz=image_size,pretrained=True,device=0,project='yolov8_robosoft2023',name='default_setting',optimizer='Adam',lr0=0.001)
    # # Validate the model
    # model = YOLO(model=trained_model,task='detect')  # load a pretrained model (recommended for training)
    # metrics = model.val(batch=1)  # no arguments needed, dataset and settings remembered

