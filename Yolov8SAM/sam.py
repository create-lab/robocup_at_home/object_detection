from segment_anything import SamAutomaticMaskGenerator, sam_model_registry
import cv2
import numpy as np
import torch
import os
import matplotlib.pyplot as plt
import time

def show_anns(anns):
    if len(anns) == 0:
        return
    sorted_anns = sorted(anns, key=(lambda x: x['area']), reverse=True)
    ax = plt.gca()
    ax.set_autoscale_on(False)

    img = np.ones((sorted_anns[0]['segmentation'].shape[0], sorted_anns[0]['segmentation'].shape[1], 4))
    img[:,:,3] = 0
    for ann in sorted_anns:
        m = ann['segmentation']
        color_mask = np.concatenate([np.random.random(3), [0.35]])
        img[m] = color_mask
    ax.imshow(img)




image_path='/home/jessica/Desktop/softrobot_competition/dataset/Own/image/IMG_1806.JPG'
sam_checkpoint = os.path.join('sam_checkpoints',"sam_vit_b_01ec64.pth")
model_type = "vit_b"
image=cv2.imread(image_path)
image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
# resize the image to half
image = cv2.resize(image, (0,0), fx=0.5, fy=0.5)
device=torch.device("cuda" if torch.cuda.is_available() else "cpu")
print(device)
sam = sam_model_registry[model_type](checkpoint=sam_checkpoint)
sam.to(device)
mask_generator = SamAutomaticMaskGenerator(sam)
print(mask_generator.point_grids)
# mask_generator= SamAutomaticMaskGenerator(
#     model=sam,
#     points_per_side=32,
#     pred_iou_thresh=0.95,
#     stability_score_thresh=0.98,
#     crop_n_layers=1,
#     crop_n_points_downscale_factor=1,
#     # min_mask_region_area=100,  # Requires open-cv to run post-processing
# )
# calculate the using time
start=time.time()    
masks = mask_generator.generate(image)
stop=time.time()
print("time:",stop-start)
plt.figure(figsize=(20,20))
plt.imshow(image)
show_anns(masks)
plt.axis('off')
plt.show() 