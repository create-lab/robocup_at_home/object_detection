import cv2
from ultralytics import YOLO
import pyzed.sl as sl
from segment_anything import SamAutomaticMaskGenerator, sam_model_registry
import cv2
import numpy as np
import torch
import os
import time
import sys
from segment_anything import sam_model_registry, SamPredictor
import random
import argparse

def generate_random_color():
    r = random.randint(50, 200)
    g = random.randint(50, 200)
    b = random.randint(50, 200)
    return r, g, b


def overlay(image, mask, alpha, resize=None):
    """Combines image and its segmentation mask into a single image.
    https://www.kaggle.com/code/purplejester/showing-samples-with-segmentation-mask-overlay

    Params:
        image: Training image. np.ndarray,
        mask: Segmentation mask. np.ndarray,
        color: Color for segmentation mask rendering.  tuple[int, int, int] = (255, 0, 0)
        alpha: Segmentation mask's transparency. float = 0.5,
        resize: If provided, both image and its mask are resized before blending them together.
        tuple[int, int] = (1024, 1024))

    Returns:
        image_combined: The combined image. np.ndarray

    """
    color = generate_random_color()
    # print('color:',color)
    # color=(255,0,0)
    color = color[::-1]
    colored_mask = np.expand_dims(mask, 0).repeat(3, axis=0)
    colored_mask = np.moveaxis(colored_mask, 0, -1)
    masked = np.ma.MaskedArray(image, mask=colored_mask, fill_value=color)
    image_overlay = masked.filled()

    if resize is not None:
        image = cv2.resize(image.transpose(1, 2, 0), resize)
        image_overlay = cv2.resize(image_overlay.transpose(1, 2, 0), resize)

    image_combined = cv2.addWeighted(image, 1 - alpha, image_overlay, alpha, 0)

    return image_combined

def get_args():
    parser = argparse.ArgumentParser(description='Real time object detection with trained Derection2 model')
    parser.add_argument('--model-weight', default='yolov8n.pt', type=str,
                        help='path to trained model weight')
    parser.add_argument('--function', default='zed', type=str,
                        help='function to run: zed or picfolder')
    parser.add_argument('--image-folder', default='', type=str,
                        help='image folder for pic folder function')
    parser.add_argument('--score-thresh', default='0.8', type=float,
                        help='Threshold score for showing the result')
    parser.add_argument('--image-size', default=512, type=int,
                        help='image size of input image')
    
    args = parser.parse_args()

    return args 


if __name__ == '__main__':
    args=get_args()

    function=args.function #'zed' or 'picfolder'

    device=torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    print(device)

    # Load the YOLOv8 model
    # model_path='/home/jessica/ros2_ws/src/object_detection/object_detection/object_detection/yolov8_rebosoft2023/default_setting/weights/best.pt'
    model_weight=args.model_weight
    model = YOLO(model_weight)
    model.to(device)

    # Load SAM model
    sam_checkpoint = os.path.join('sam_checkpoints',"sam_vit_b_01ec64.pth")
    model_type = "vit_b"
    sam = sam_model_registry[model_type](checkpoint=sam_checkpoint)
    sam.to(device=device)
    sam_predictor = SamPredictor(sam)

    if function =='zed':
        # Set up zed camera
        # Create a ZED camera object
        zed = sl.Camera()

        # Set configuration parameters
        init_params = sl.InitParameters()
        init_params.camera_resolution = sl.RESOLUTION.HD1080  # Use HD1080 video mode
        init_params.depth_mode = sl.DEPTH_MODE.PERFORMANCE
        init_params.depth_minimum_distance = 100 # min distance 100 for zed mini/ min distance 300 for zed2

        # Open the camera
        err = zed.open(init_params)
        if err != sl.ERROR_CODE.SUCCESS:
            print('error with camera')
            exit(-1)

        # Set camera runtime parameter
        runtime_parameters = sl.RuntimeParameters()
        runtime_parameters.confidence_threshold = 50

        # half-resolution images, increase speed
        image_size = zed.get_camera_information().camera_resolution


        image_size.width = image_size.width / 2
        image_size.height = image_size.height / 2

        # Create an RGBA sl.Mat object
        image_zed = sl.Mat()
        point_cloud = sl.Mat()

    while True:
        if function=='zed':
            if zed.grab() == sl.ERROR_CODE.SUCCESS:
                
                # Retrieve the left image in sl.Mat
                zed.retrieve_image(image_zed, sl.VIEW.LEFT,sl.MEM.CPU,image_size)
                # Use get_data() to get the numpy array
                image = image_zed.get_data()
                image = image[:, :, :3]
                ori_img=image.copy()
                image_with_masks=image.copy()
                # Retrieve colored point cloud. Point cloud is aligned on the left image.
                zed.retrieve_measure(point_cloud, sl.MEASURE.XYZRGBA,sl.MEM.CPU,image_size)
                point_cloud_data = point_cloud.get_data()[:, :, :3]
                start_time = time.time()
                image=torch.from_numpy(np.expand_dims(np.moveaxis(image, -1, 0), axis=0))
                image.to(device=device)
                # Set the image to the sam predictor
                sam_predictor.set_image(ori_img)
                
                # Run YOLOv8 inference on the frame
                results = model.predict(image, device=device)
               
                names=results[0].names

                output_boxes=[]
                output_name=[]
                output_mask=[]
              
                for i in range(len(results[0].boxes)):
                    # print(results[0].boxes[i])
                    input_box = results[0].boxes[i].xyxy.cpu().numpy()[0]
                    print('input_box:',input_box)
                    predicted_class=results[0].boxes[i].cls.cpu().numpy()
                    print('predicted_class:',predicted_class[0])
                    name =names[predicted_class[0]]
                    print('name:',name)
                    predicted_confidence=results[0].boxes[i].conf.cpu().numpy()[0]
                    print('predicted_confidence:',predicted_confidence)
                    if predicted_confidence<args.score_thresh:
                        continue
                    # input point is the midlle of input box
                    input_point=np.array([(input_box[0]+input_box[2])/2,(input_box[1]+input_box[3])/2])
                    # mask: an array of False and True
                    mask, mask_scores, logits = sam_predictor.predict(
                        point_coords=np.expand_dims(input_point, axis=0),
                        point_labels=np.array([1]),#  labels 1 (foreground point) or 0 (background point)
                        box=input_box[None, :], # [x0, y0, x1, y1]
                        multimask_output=False,
                    )
                    output_boxes.append([(int(input_box[0]), int(input_box[1])), (int(input_box[2]), int(input_box[3]))])
                    output_name.append(name)
                    output_mask.append(mask)
                
                fps_caption = "FPS: {:.0f}".format(1 / (time.time() - start_time))
                print(fps_caption)

                
                areas = [(max_x - min_x) * (max_y - min_y) for (min_x, min_y), (max_x, max_y) in output_boxes]

                # Sort masks by bounding box area in descending order
                sorted_masks = [mask for _, mask in sorted(zip(areas, output_mask), key=lambda x: x[0], reverse=True)]

                for i in range(len(sorted_masks)):
                    image_with_masks = overlay(image_with_masks, sorted_masks[i][0], alpha=1)
                for i in range(len(output_boxes)):
                    image_with_masks=cv2.rectangle(image_with_masks, output_boxes[i][0], output_boxes[i][1], (0, 255, 0), 2)
                    image_with_masks = cv2.putText(image_with_masks, output_name[i], (output_boxes[i][0][0]+10, output_boxes[i][0][1]+10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255,0,0), 1, cv2.LINE_AA)

                cv2.imshow("ori_img", ori_img)
                cv2.imshow("SAM Inference", image_with_masks)
                
                
                fps_caption = "FPS: {:.0f}".format(1 / (time.time() - start_time))
                print(fps_caption)

                # Break the loop if 'q' is pressed
                if cv2.waitKey(1) == ord('q'):
                    cv2.destroyAllWindows()
                    break               
                    
                
        elif function=='picfolder':
            print('pic folder')
            # # # # # # # # # # # # # # # pic folder 
            # validation_path='/home/jessica/Desktop/softrobot_competition/dataset/onebyone.v1i.coco-segmentation/valid/yolo_labels_valid/images'
            validation_path=args.image_folder
            for f in os.listdir(validation_path):
                image_path=os.path.join(validation_path,f)
                print('image_path:',image_path)
                image=cv2.imread(image_path)
                w,h=image.shape[1],image.shape[0]

                image=cv2.resize(image,(args.image_size,args.image_size))
                print('image shape:',image.shape)
                ori_img=image.copy()
                image_with_masks=image.copy()

                start_time = time.time()

                image=torch.from_numpy(np.expand_dims(np.moveaxis(image, -1, 0), axis=0))
                image.to(device=device)
                # Set the image to the sam predictor
                sam_predictor.set_image(ori_img)
                print('image.shape:',image.shape)
                # Run YOLOv8 inference on the frame
                results = model.predict(image, device=device)

                names=results[0].names

                output_boxes=[]
                output_name=[]
                output_mask=[]
                for i in range(len(results[0].boxes)):
                    # print(results[0].boxes[i])
                    input_box = results[0].boxes[i].xyxy.cpu().numpy()[0]
                    print('input_box:',input_box)
                    predicted_class=results[0].boxes[i].cls.cpu().numpy()
                    print('predicted_class:',predicted_class[0])
                    name =names[predicted_class[0]]
                    print('name:',name)
                    predicted_confidence=results[0].boxes[i].conf.cpu().numpy()[0]
                    if predicted_confidence<args.score_thresh:
                        continue
                    print('predicted_confidence:',predicted_confidence)
                    # input point is the midlle of input box
                    input_point=np.array([(input_box[0]+input_box[2])/2,(input_box[1]+input_box[3])/2])
                    print('input_point:',input_point.shape)
                    print('predicted_class:',predicted_class.shape)
                    # mask: an array of False and True
                    mask, mask_scores, logits = sam_predictor.predict(
                        point_coords=np.expand_dims(input_point, axis=0),
                        point_labels=np.array([1]),#  labels 1 (foreground point) or 0 (background point)
                        box=input_box[None, :], # [x0, y0, x1, y1]
                        multimask_output=False,
                    )

                    output_boxes.append([(int(input_box[0]), int(input_box[1])), (int(input_box[2]), int(input_box[3]))])
                    output_name.append(name)
                    output_mask.append(mask)
                
                fps_caption = "FPS: {:.0f}".format(1 / (time.time() - start_time))
                print(fps_caption)

                
                areas = [(max_x - min_x) * (max_y - min_y) for (min_x, min_y), (max_x, max_y) in output_boxes]

                # Sort masks by bounding box area in descending order
                sorted_masks = [mask for _, mask in sorted(zip(areas, output_mask), key=lambda x: x[0], reverse=True)]

                for i in range(len(sorted_masks)):
                    image_with_masks = overlay(image_with_masks, sorted_masks[i][0], alpha=1)
                for i in range(len(output_boxes)):
                    image_with_masks=cv2.rectangle(image_with_masks, output_boxes[i][0], output_boxes[i][1], (0, 255, 0), 2)
                    image_with_masks = cv2.putText(image_with_masks, output_name[i], (output_boxes[i][0][0]+10, output_boxes[i][0][1]+10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255,0,0), 1, cv2.LINE_AA)

                ori_img=cv2.resize(ori_img,(int(w),int(h)))
                image_with_masks=cv2.resize(image_with_masks,(int(w),int(h)))
                cv2.imshow("ori_img", ori_img)
                cv2.imshow("SAM Inference", image_with_masks)
                
                
                fps_caption = "FPS: {:.0f}".format(1 / (time.time() - start_time))
                print(fps_caption)
                
                if cv2.waitKey(0) == ord('q'):
                    cv2.destroyAllWindows()
                    exit()
        else:
            print('Please choose function: zed or picfolder')
            exit()