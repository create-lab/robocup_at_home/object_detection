from detectron2.data.datasets import register_coco_instances
# from detectron2.data import DatasetCatalog
from detectron2.data import MetadataCatalog
# from detectron2.data import detection_utils as utils
from detectron2.engine import DefaultTrainer, BestCheckpointer#, default_argument_parser, default_setup, launch
from detectron2.data import DatasetMapper, MetadataCatalog, build_detection_train_loader
from detectron2.utils.visualizer import Visualizer
from detectron2.config import get_cfg
from detectron2.checkpoint import DetectionCheckpointer
# import matplotlib.pyplot as plt
import cv2
import os
import numpy as np
# from typing import List, Optional, Union
# import torch
from detectron2.config import configurable
from detectron2.data import transforms as T
import math
from detectron2.utils.logger import log_every_n_seconds
# import detectron2.utils.comm as comm
import datetime
from fvcore.transforms.transform import Transform, NoOpTransform
import albumentations as A
from detectron2.data.transforms import Augmentation

from detectron2.evaluation import (
    CityscapesInstanceEvaluator,
    CityscapesSemSegEvaluator,
    COCOEvaluator,
    COCOPanopticEvaluator,
    DatasetEvaluators,
    LVISEvaluator,
    PascalVOCDetectionEvaluator,
    SemSegEvaluator,
    verify_results,
)
from omegaconf import OmegaConf
import argparse
# import random

def build_evaluator(cfg, dataset_name, output_folder=None):
    """
    Create evaluator(s) for a given dataset.
    This uses the special metadata "evaluator_type" associated with each builtin dataset.
    For your own dataset, you can simply create an evaluator manually in your
    script and do not have to worry about the hacky if-else logic here.
    """
    if output_folder is None:
        output_folder = os.path.join(cfg.OUTPUT_DIR, "inference")
    evaluator_list = []
    evaluator_type = MetadataCatalog.get(dataset_name).evaluator_type
    if evaluator_type in ["sem_seg", "coco_panoptic_seg"]:
        evaluator_list.append(
            SemSegEvaluator(
                dataset_name,
                distributed=True,
                output_dir=output_folder,
            )
        )
    if evaluator_type in ["coco", "coco_panoptic_seg"]:
        evaluator_list.append(COCOEvaluator(dataset_name, output_dir=output_folder))
    if evaluator_type == "coco_panoptic_seg":
        evaluator_list.append(COCOPanopticEvaluator(dataset_name, output_folder))
    if evaluator_type == "cityscapes_instance":
        return CityscapesInstanceEvaluator(dataset_name)
    if evaluator_type == "cityscapes_sem_seg":
        return CityscapesSemSegEvaluator(dataset_name)
    elif evaluator_type == "pascal_voc":
        return PascalVOCDetectionEvaluator(dataset_name)
    elif evaluator_type == "lvis":
        return LVISEvaluator(dataset_name, output_dir=output_folder)
    if len(evaluator_list) == 0:
        raise NotImplementedError(
            "no Evaluator for the dataset {} with the type {}".format(dataset_name, evaluator_type)
        )
    elif len(evaluator_list) == 1:
        return evaluator_list[0]
    return DatasetEvaluators(evaluator_list)


class AlbumentationsTransform(Transform):
    def __init__(self, aug, params):
        self.aug = aug
        self.params = params

    def apply_coords(self, coords: np.ndarray) -> np.ndarray:
        return coords

    def apply_image(self, image):
        return self.aug.apply(image, **self.params)

    def apply_box(self, box: np.ndarray) -> np.ndarray:
        try:
            return np.array(self.aug.apply_to_bboxes(box.tolist(), **self.params))
        except AttributeError:
            return box

    def apply_segmentation(self, segmentation: np.ndarray) -> np.ndarray:
        try:
            return self.aug.apply_to_mask(segmentation, **self.params)
        except AttributeError:
            return segmentation

class AlbumentationsWrapper(Augmentation):
    """
    Wrap an augmentor form the albumentations library: https://github.com/albu/albumentations.
    Image, Bounding Box and Segmentation are supported.
    Example:
    .. code-block:: python
        import albumentations as A
        from detectron2.data import transforms as T
        from detectron2.data.transforms.albumentations import AlbumentationsWrapper

        augs = T.AugmentationList([
            AlbumentationsWrapper(A.RandomCrop(width=256, height=256)),
            AlbumentationsWrapper(A.HorizontalFlip(p=1)),
            AlbumentationsWrapper(A.RandomBrightnessContrast(p=1)),
        ])  # type: T.Augmentation

        # Transform XYXY_ABS -> XYXY_REL
        h, w, _ = IMAGE.shape
        bbox = np.array(BBOX_XYXY) / [w, h, w, h]

        # Define the augmentation input ("image" required, others optional):
        input = T.AugInput(IMAGE, boxes=bbox, sem_seg=IMAGE_MASK)

        # Apply the augmentation:
        transform = augs(input)
        image_transformed = input.image  # new image
        sem_seg_transformed = input.sem_seg  # new semantic segmentation
        bbox_transformed = input.boxes   # new bounding boxes

        # Transform XYXY_REL -> XYXY_ABS
        h, w, _ = image_transformed.shape
        bbox_transformed = bbox_transformed * [w, h, w, h]
    """

    def __init__(self, augmentor):
        """
        Args:
            augmentor (albumentations.BasicTransform):
        """
        # super(Albumentations, self).__init__() - using python > 3.7 no need to call rng
        self._aug = augmentor

    def get_transform(self, image):
        do = self._rand_range() < self._aug.p
        if do:
            params = self.prepare_param(image)
            return AlbumentationsTransform(self._aug, params)
        else:
            return NoOpTransform()

    def prepare_param(self, image):
        params = self._aug.get_params()
        if self._aug.targets_as_params:
            targets_as_params = {"image": image}
            params_dependent_on_targets = self._aug.get_params_dependent_on_targets(targets_as_params)
            params.update(params_dependent_on_targets)
        params = self._aug.update_params(params, **{"image": image})
        return params

class Trainer(DefaultTrainer):
    @classmethod
    def build_evaluator(cls, cfg, dataset_name, output_folder=None):
        return build_evaluator(cfg, dataset_name, output_folder)

    def build_hooks(self):
        cfg = self.cfg.clone()
        hooks = super().build_hooks()
        hooks.insert(-1, BestCheckpointer(cfg.TEST.EVAL_PERIOD,
                                          DetectionCheckpointer(self.model, cfg.OUTPUT_DIR),
                                          'bbox/AP', # "bbox/AP50",
                                          "max",
                                          ))

        return hooks

    # @classmethod
    # def build_test_loader(cls, cfg, dataset_name):
    #     return build_detection_test_loader(cfg, dataset_name, mapper=DatasetMapper(cfg, False))

    # @classmethod
    def build_train_loader(cls, cfg):
        return build_detection_train_loader(cfg, mapper=DatasetMapper(cfg,is_train=True,augmentations=[
            T.ResizeShortestEdge(short_edge_length=(640, 672, 704, 736, 768, 800), max_size=1333, sample_style='choice'),
            T.RandomApply(T.RandomBrightness(0.2, 1.8), 0.5),
            T.RandomApply(T.RandomContrast(0.2, 1.8), 0.5),
            T.RandomApply(T.RandomSaturation(0.2, 1.8), 0.5),
            T.RandomApply(T.RandomLighting(300), 0.6),
            T.RandomFlip(prob=0.3, horizontal=True, vertical=False),
            T.RandomFlip(prob=0.3, horizontal=False, vertical=True),
            # AlbumentationsWrapper(A.Cutout(num_holes=30, max_h_size=50, max_w_size=50, fill_value=0, p=0.3)),
            # T.RandomApply(T.RandomRotation(angle=(90, -90)),0.05)
         ]))



def Visualize_augmented_image(cfg,trainer,dataset):
    ############visulize augmented image############
    loader = trainer.build_train_loader(cfg)
    i=0
    metadata = MetadataCatalog.get(dataset)
    for sample_image_batch_idx, train_image_batch in enumerate(loader):
        for idx, train_image in enumerate(train_image_batch):
            i+=1
            image = train_image["image"].numpy().transpose(1, 2, 0)
    
            target_fields = train_image["instances"].get_fields()
            labels = [metadata.thing_classes[i] for i in target_fields["gt_classes"]]
    
            # visualize ground truth
            gt_visualizer = Visualizer(
                image[:, :, ::-1], metadata=metadata, scale=1
            )
            gt_image_vis = gt_visualizer.overlay_instances(
                labels=labels,
                boxes=target_fields.get("gt_boxes", None),
                # masks=target_fields.get("gt_masks", None),
                keypoints=target_fields.get("gt_keypoints", None),
            )
    
            # cv2.imwrite(f'augmented_image{i}.jpg',gt_image_vis.get_image()[:, :, ::-1])
            cv2.imshow('image', gt_image_vis.get_image()[:, :, ::-1])
            if cv2.waitKey(0) == ord('q'):
                cv2.destroyAllWindows()
                return

def custom_config(config):
    cfg = get_cfg()

    cfg.merge_from_file(config.MODEL_FILE_PATH)

    cfg.DATALOADER.NUM_WORKERS = config.DATALOADER.NUM_WORKERS
    cfg.MODEL.WEIGHTS = config.MODEL.WEIGHTS
    cfg.SOLVER.IMS_PER_BATCH = config.SOLVER.IMS_PER_BATCH
    cfg.SOLVER.BASE_LR = config.SOLVER.BASE_LR
    cfg.SOLVER.GAMMA = config.SOLVER.GAMMA
    # The iteration number to decrease learning rate by GAMMA.
    cfg.SOLVER.STEPS = list(config.SOLVER.STEPS)
    

    iter_each_epoch=config.TOTAL_TRAIN_IMAGE/cfg.SOLVER.IMS_PER_BATCH
    max_iter=iter_each_epoch*config.EPOCH
    cfg.SOLVER.MAX_ITER =int(max_iter)

    cfg.MODEL.ROI_HEADS.NUM_CLASSES = config.MODEL.ROI_HEADS.NUM_CLASSES
    # Save a checkpoint after every this number of iterations
    cfg.SOLVER.CHECKPOINT_PERIOD= int(max_iter+1)
    cfg.TEST.EVAL_PERIOD =int(iter_each_epoch)   #put the number of iteration for each epoch
    #1 epoch = int((total_image / cfg.SOLVER.IMS_PER_BATCH)) iteration

    
    cfg.DATALOADER.SAMPLER_TRAIN = config.DATALOADER.SAMPLER_TRAIN 
    cfg.DATALOADER.REPEAT_THRESHOLD = config.DATALOADER.REPEAT_THRESHOLD 

    # DATASETS
    cfg.DATASETS.TRAIN = (config.DATASETS.TRAIN,)
    cfg.DATASETS.TEST = (config.DATASETS.TEST,)
   

    # DATASETS
    # add datetime to folder name
    cfg.OUTPUT_DIR =f'trained_model/train_output_batch{cfg.SOLVER.IMS_PER_BATCH}_epoch{config.EPOCH}_lr{cfg.SOLVER.BASE_LR}_{datetime.datetime.now().strftime("%Y%m%d_%H%M%S")}'
    
    return cfg

def get_args():
    parser = argparse.ArgumentParser(description='Train detectron2 model')
    parser.add_argument('--cfg', default='train_config.yaml', type=str,
                        help='path to config file for setting hyperparameters')
    parser.add_argument('--train', action='store_true',
                        help='train the model')
    parser.add_argument('--visulize-augmented-image', action='store_true',
                        help='visulize augmented image')
    parser.add_argument('--visulize-dataset', default='', type=str,
                        help='dataset you want to visulize: it should be same name in config file (TRAIN:xxx or TEST:xxx)')
    args = parser.parse_args()

    return args          

if __name__ == '__main__':
    args = get_args()
    config = OmegaConf.load(args.cfg)

    if not args.train and not args.visulize_augmented_image:
        print('Please choose one: --train or --visulize-augmented-image')
        exit()
   
    register_coco_instances(config.DATASETS.TRAIN, {},config.TRAIN_ANNO_PATH,config.TRAIN_IMAGE_PATH)
    register_coco_instances(config.DATASETS.TEST, {},config.VALID_ANNO_PATH,config.VALID_IMAGE_PATH)

    cfg = custom_config(config)

    trainer = Trainer(cfg)
    
    if args.train:
        trainer.resume_or_load(resume=False)
        trainer.train()

        # save the config file into output folder
        # os.makedirs(cfg.OUTPUT_DIR, exist_ok=True)
        cfg_path = os.path.join(cfg.OUTPUT_DIR, "config.yaml")
        # print(cfg.dump())
        # OmegaConf.save(cfg.dump(), f)
        # Save the configuration to the output file
        with open(cfg_path, "w") as f:
            cfg.dump(stream=f)

    elif args.visulize_augmented_image:
        dataset=args.visulize_dataset
        Visualize_augmented_image(cfg,trainer,dataset)
   