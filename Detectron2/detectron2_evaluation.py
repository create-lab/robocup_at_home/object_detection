#import the COCO Evaluator to use the COCO Metrics
from detectron2.config import get_cfg
from detectron2.engine import DefaultPredictor
from detectron2.data import build_detection_test_loader
from detectron2.data.datasets import register_coco_instances
from detectron2.evaluation import COCOEvaluator, inference_on_dataset
from detectron2.engine import DefaultTrainer
import argparse
from omegaconf import OmegaConf


def custom_config(config,model_weight=None):
    #load the config file, configure the threshold value, load weights 
    cfg = get_cfg()
    cfg.merge_from_file(config.MODEL_FILE_PATH)
    # cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = 0.5  # set threshold for this model
    # cfg.MODEL.WEIGHTS = "/home/jessica/ros2_ws/src/object_detection/object_detection/object_detection/Detectron2/trained_model/train_output_batch4_epoch50_lr0.001_202303310127/model_best_train_output_batch4_epoch50_lr0.001_202303310127.pth"
    cfg.MODEL.WEIGHTS = model_weight
    cfg.MODEL.ROI_HEADS.NUM_CLASSES = config.MODEL.ROI_HEADS.NUM_CLASSES
    # DATASETS
    cfg.DATASETS.TEST = (config.DATASETS.TEST,)

    return cfg

def get_args():
    parser = argparse.ArgumentParser(description='Evaluate trained detectron2 model')
    parser.add_argument('--cfg', default='train_config.yaml', type=str,
                        help='path to config file for setting hyperparameters')
    parser.add_argument('--model-weight', default='', type=str,
                        help='path to the trained model weight')
    args = parser.parse_args()

    return args   

if __name__ == '__main__':
    args = get_args()
    config = OmegaConf.load(args.cfg)

    #register your data
    # register_coco_instances("valid1", {},
    #                             "/home/jessica/Desktop/softrobot_competition/dataset/onebyone.v1i.coco-segmentation/valid/annotations/_annotations_bunch_greenbeans.coco.json",
    #                             "/home/jessica/Desktop/softrobot_competition/dataset/onebyone.v1i.coco-segmentation/valid/images")

    register_coco_instances(config.DATASETS.TEST, {},config.VALID_ANNO_PATH,config.VALID_IMAGE_PATH)

    model_weight=args.model_weight
    cfg=custom_config(config,model_weight=model_weight)

    # Create predictor
    predictor = DefaultPredictor(cfg)

    #Call the COCO Evaluator function and pass the Validation Dataset
    evaluator = COCOEvaluator("valid1", output_dir="./evaluation_output/")
    val_loader = build_detection_test_loader(cfg, "valid1")

    #Use the created predicted model in the previous step
    inference_on_dataset(predictor.model, val_loader, evaluator)