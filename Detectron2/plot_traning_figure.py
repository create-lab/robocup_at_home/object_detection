from matplotlib.pylab import plt
from numpy import arange
import json
import numpy as np

json_path = '/home/jessica/Desktop/softrobot_competition/trained_model/train_output_batch4_epoch50_lr0.001_202304011946/metrics_train_output_batch4_epoch50_lr0.001_202304011946.json'
# f = open(json_path)
# # returns JSON object as
# # a dictionary
# data = json.load(f)
train1_AP=[]
valid1_AP=[]
with open(json_path) as f:
    datas = [json.loads(line) for line in f]
# datas.pop()
# datas.pop()
for _, data in enumerate(datas):
    try:

        train1_AP.append(data["train1/bbox/AP"])
        # train1_AP.append(data["train1/bbox/AP-greenbeans"])
        valid1_AP.append(data["valid1/bbox/AP"])
        # valid1_AP.append(data["valid1/bbox/AP-greenbeans"])
    except:
        pass
print('epoch of best model:',np.argmax(valid1_AP)+1)
numofepochs= len(train1_AP)+1
# Generate a sequence of integers to represent the epoch numbers
epochs = range(1,numofepochs)

# Plot and label the training and validation loss values
plt.plot(epochs, train1_AP, label='Training AP')
plt.plot(epochs, valid1_AP, label='Validation AP')

# Add in a title and axes labels
plt.title('Training and Validation AP')
plt.xlabel('Epochs')
plt.ylabel('AP')

# Set the tick locations
plt.xticks(arange(0, numofepochs, 2))

# Display the plot
plt.legend(loc='best')
plt.show()