import argparse
import glob
import multiprocessing as mp
import numpy as np
import os
import tempfile
import time
import warnings
from omegaconf import OmegaConf

import cv2
import pyzed.sl as sl
import torch
import math
import statistics
import tqdm
from detectron2.config import get_cfg
from detectron2.engine.defaults import DefaultPredictor
from detectron2.data import MetadataCatalog
from detectron2.data.detection_utils import read_image
from detectron2.utils.logger import setup_logger
import imageio
from detectron2.data.datasets import register_coco_instances
from demo.predictor import VisualizationDemo
from detectron2.utils.video_visualizer import VideoVisualizer

class zed_VisualizationDemo(VisualizationDemo):
    def process_predictions_zed(self,frame, predictions):
        video_visualizer = VideoVisualizer(self.metadata, self.instance_mode)

        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        if "panoptic_seg" in predictions:
            panoptic_seg, segments_info = predictions["panoptic_seg"]
            vis_frame = video_visualizer.draw_panoptic_seg_predictions(
                frame, panoptic_seg.to(self.cpu_device), segments_info
            )

        elif "instances" in predictions:
            predictions = predictions["instances"].to(self.cpu_device)
            vis_frame = video_visualizer.draw_instance_predictions(frame, predictions)

        elif "sem_seg" in predictions:
            vis_frame = video_visualizer.draw_sem_seg(
                frame, predictions["sem_seg"].argmax(dim=0).to(self.cpu_device)
            )

        # Converts Matplotlib RGB format to OpenCV BGR format
        vis_frame = cv2.cvtColor(vis_frame.get_image(), cv2.COLOR_RGB2BGR)
        return vis_frame, predictions


def calculate_distance(point_cloud,boundingbox):
    # if centerpoint:
    #only calculate the ditance of the center point of object
    # get the 3d position of center pixel
    x1, y1, x2, y2 = boundingbox
    x = round((x1+x2)/ 2)
    y = round((y1+y2) / 2)
    point_cloud_value = point_cloud[y,x]
    avg_distance = math.sqrt(point_cloud_value[0] * point_cloud_value[0] +
                         point_cloud_value[1] * point_cloud_value[1] +
                         point_cloud_value[2] * point_cloud_value[2])

    return round(avg_distance,2),[point_cloud_value[0],point_cloud_value[1],point_cloud_value[2]]


def prediction_result(predictions,point_cloud,draw_position=False,image=None):
    """
        boxes: [num_instance, (x1, y1, x2, y2)] in image coordinates.
        masks: [num_instances,height, width]
        class_ids: [num_instances]
        scores: (optional) confidence scores for each box
        """

    boxes = predictions.pred_boxes.tensor.numpy() if predictions.has("pred_boxes") else None
    scores = predictions.scores.numpy() if predictions.has("scores") else None
    class_ids = predictions.pred_classes.numpy() if predictions.has("pred_classes") else None
    masks = predictions.pred_masks.numpy() if predictions.has("pred_masks") else None
    class_names = MetadataCatalog.get(cfg.DATASETS.TEST[0]).thing_classes
    pred_class_names = list(map(lambda x: class_names[x], class_ids))
    result=[]
    # Number of instances
    N = boxes.shape[0]
    if not N:
        print("\n*** No instances to display *** \n")
    else:
        print("\n*** Detect {} objects *** \n".format(N))
        assert boxes.shape[0] == masks.shape[0] == class_ids.shape[0]


    for i in range(N):
        dict={}

        if point_cloud is not None:
            # compute 3d position
            distance, position = calculate_distance(point_cloud, boxes[i])
            dict['position'] =position

        # x1, y1, x2, y2 = boxes[i].astype(int)
        dict['box'] = [boxes[i].astype(int)]
        dict['class_id'] = class_ids[i]
        dict['class_name']=pred_class_names[i]
        dict['scores'] = scores[i]
        dict['mask']=masks[i]


        result.append(dict)

        if draw_position:
            x1, y1, x2, y2 = boxes[i].astype(int)

            # Mask
            mask = masks[i, :, :]

            if point_cloud is not None:
                # compute 3d position
                distance_text = str(distance) + "mm" + " " + str(position[0]) + " " + str(position[1]) + " " + str(
                    position[2])
                t_size = cv2.getTextSize(distance_text, 0, fontScale=0.5, thickness=1)[0]
                c2 = x1 + t_size[0], y1 + t_size[1] + 3
                cv2.rectangle(image, (x1, y1), c2, [255, 255, 255], -1, cv2.LINE_AA)  # filled
                cv2.putText(image, distance_text, (x1, y1 + t_size[1]), 0, 0.5, [0, 0, 0], thickness=1,
                            lineType=cv2.LINE_AA)


    return result,image

def set_zed():
    # Create a ZED camera object
    zed = sl.Camera()

    # Set configuration parameters
    init_params = sl.InitParameters()
    init_params.camera_resolution = sl.RESOLUTION.HD720
    init_params.depth_mode = sl.DEPTH_MODE.PERFORMANCE
    init_params.depth_minimum_distance = 100 # min distance 100 for zed mini/ min distance 300 for zed2

    # Open the camera
    err = zed.open(init_params)
    if err != sl.ERROR_CODE.SUCCESS:
        print('error with camera')
        exit(-1)

    # Set camera runtime parameter
    runtime_parameters = sl.RuntimeParameters()
    runtime_parameters.confidence_threshold = 50

    # half-resolution images, increase speed
    image_size = zed.get_camera_information().camera_resolution


    # image_size.width = image_size.width / 2
    # image_size.height = image_size.height / 2

    # Create an RGBA sl.Mat object
    image_zed = sl.Mat()
    point_cloud = sl.Mat()

    return zed, image_size, image_zed, point_cloud


def get_args():
    parser = argparse.ArgumentParser(description='Real time object detection with trained Derection2 model')
    parser.add_argument('--cfg', default='train_config.yaml', type=str,
                        help='path to config file for setting hyperparameters')
    parser.add_argument('--model-weight', default='detectron2://COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x/137849600/model_final_f10217.pkl', type=str,
                        help='path to trained model weight')
    parser.add_argument('--function', default='zed', type=str,
                        help='function to run: zed, pic, picfolder, zedpic')
    parser.add_argument('--score-thresh', default='0.8', type=float,
                        help='Threshold score for showing the result')
    parser.add_argument('--draw', action='store_false',
                        help='draw the result for zed and zed pic function')
    parser.add_argument('--image-path', default='', type=str,
                        help='image path for pic function')
    parser.add_argument('--image-folder', default='', type=str,
                        help='image folder for pic folder function')
    
    args = parser.parse_args()

    return args 

def custom_config(config, model_weight=None,score_thresh=0.8):

    cfg = get_cfg()
    cfg.merge_from_file(config.MODEL_FILE_PATH)
    cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = score_thresh
    cfg.MODEL.WEIGHTS = model_weight
    cfg.MODEL.ROI_HEADS.NUM_CLASSES = config.MODEL.ROI_HEADS.NUM_CLASSES# differnt model may have differnt number of class
    cfg.DATASETS.TEST = ("data",)

    # Define the class names
    class_names=list(config.CLASS_NAME)
    MetadataCatalog.get(cfg.DATASETS.TEST[0]).thing_classes=class_names
    #MetadataCatalog.get('data').thing_classes=['bottle','bowls','box','broccoli','carrots','cookie','cups','eggs','foodcontainers','greenbeans','meatballs','orangejuice','plates','sausage','spaghettinoodles','tray','icegems']

    cfg.freeze()

    return cfg

if __name__=='__main__':
    args=get_args()
    config = OmegaConf.load(args.cfg)

    cfg=custom_config(config, model_weight=args.model_weight,score_thresh=args.score_thresh)

    predictor = DefaultPredictor(cfg)
    if args.draw:
        demo = zed_VisualizationDemo(cfg)
    if args.function=='zed' or args.function=='zedpic':
        zed, image_size, image_zed, point_cloud=set_zed()

    start_time = time.time()
    # writer= cv2.VideoWriter('result/mask_pc.mp4', cv2.VideoWriter_fourcc(*'XVID'), 5, (image_size.width, image_size.height))
    # frames = []

    if args.function=='zed':
        print('zed')
        print("image size:", image_size.width, image_size.height)
        while True:
            if zed.grab() == sl.ERROR_CODE.SUCCESS:
                # Retrieve the left image in sl.Mat
                zed.retrieve_image(image_zed, sl.VIEW.LEFT,sl.MEM.CPU,image_size)
                # Use get_data() to get the numpy array
                image = image_zed.get_data()
                image = image[:, :, :3]

                # Retrieve colored point cloud. Point cloud is aligned on the left image.
                zed.retrieve_measure(point_cloud, sl.MEASURE.XYZRGBA,sl.MEM.CPU,image_size)
                point_cloud_data = point_cloud.get_data()[:, :, :3]

                prediction = predictor(image)

                if args.draw:
                    mask_img, predictions = demo.process_predictions_zed(image, prediction)
                else:
                    predictions=prediction["instances"].to(torch.device("cpu"))
                    mask_img=None

                result,mask_img=prediction_result(predictions, point_cloud_data, draw_position=args.draw, image=mask_img)
                detected_class_name=[sub['class_name'] for sub in result]
                detected_scores=[sub['scores'] for sub in result]
                print('object detected:',str(detected_class_name))
                print('object detected score:', str(detected_scores))
                if args.draw:
                    cv2.imshow("mask", mask_img)
                    # cv2.waitKey(0)
                    # cv2.imwrite("result/mask_pc.png",mask_img)
                    # writer.write(mask_img)
                    # frames.append(mask_img)

                cv2.imshow("original", image)
                fps_caption = "FPS: {:.0f}".format(1 / (time.time() - start_time))
                print(fps_caption)
                start_time = time.time()

            if cv2.waitKey(1) & 0xFF == ord('q'):
                break



    elif args.function=='pic':
        print('pic')
        # # # # # # # # # # # # # # # pic
        image_path=args.image_path
        # print('image_path:',image_path)
        image=cv2.imread(image_path)

        # # resize image
        # scale_percent = 30  # percent of original size
        # width = int(image.shape[1] * scale_percent / 100)
        # height = int(image.shape[0] * scale_percent / 100)
        # dim = (width, height)
        # image = cv2.resize(image, dim, interpolation=cv2.INTER_AREA)
        
        predictions, vis_output = demo.run_on_image(image)
        # convert back to BGR for cv2
        result_image = cv2.cvtColor(vis_output.get_image(), cv2.COLOR_RGB2BGR)

        cv2.imshow("mask", result_image)
        cv2.waitKey(0)


    elif args.function=='picfolder':
        print('pic folder')
        # # # # # # # # # # # # # # # pic folder 
        image_folder_path=args.image_folder
        for f in os.listdir(image_folder_path):
            image_path=os.path.join(image_folder_path,f)
            # print('image_path:',image_path)
            image=cv2.imread(image_path)
            print('image shape:',image.shape)
            # # resize image
            # scale_percent = 30  # percent of original size
            # width = int(image.shape[1] * scale_percent / 100)
            # height = int(image.shape[0] * scale_percent / 100)
            # dim = (width, height)
            # image = cv2.resize(image, (512,512), interpolation=cv2.INTER_AREA)
            start_time = time.time()
            predictions, vis_output = demo.run_on_image(image)
            result_image = cv2.cvtColor(vis_output.get_image(), cv2.COLOR_RGB2BGR)
            
            fps_caption = "FPS: {:.0f}".format(1 / (time.time() - start_time))
            print(fps_caption)
            
            cv2.imshow("mask", result_image)
            if cv2.waitKey(0) == ord('q'):
                break

    elif args.function=='zedpic':
        print('zed pic')
        # # # # # # # # # # # # # # zed pic
        if zed.grab() == sl.ERROR_CODE.SUCCESS:
            zed.retrieve_image(image_zed, sl.VIEW.LEFT, sl.MEM.CPU, image_size)
            # Use get_data() to get the numpy array
            image = image_zed.get_data()
            image = image[:, :, :3]
            prediction = predictor(image)
        
            # Retrieve colored point cloud. Point cloud is aligned on the left image.
            zed.retrieve_measure(point_cloud, sl.MEASURE.XYZRGBA, sl.MEM.CPU, image_size)
            point_cloud_data = point_cloud.get_data()[:, :, :3]
            if args.draw:
                    mask_img, predictions = demo.process_predictions_zed(image, prediction)
            else:
                predictions=prediction["instances"].to(torch.device("cpu"))
                mask_img=None
            result ,mask_img= prediction_result(predictions, point_cloud_data, draw_position=args.draw, image=mask_img)
            print(result)
        
            cv2.imshow("mask", mask_img)
            cv2.waitKey(0)
    else:
        print('Please choose a function: zed, pic, picfolder, zedpic')


    cv2.destroyAllWindows()
    if args.function=='zed' or args.function=='zedpic':
        zed.close()
    # Convert to gif using the imageio.mimsave method
    # imageio.mimsave('result/mask_pc.gif', frames, fps=5)
    # writer.release()

