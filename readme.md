The work is based on [Detectron2](https://github.com/facebookresearch/detectron2), [YOLOv8](https://github.com/ultralytics/ultralytics), [Segment Anything](https://github.com/facebookresearch/segment-anything).

# Installation

Ubuntu 22

ROS 2 Humble

Python 3.10

Pytorch 1.13.1+cu117

##  Zed2
See [Install ZED SDK on Linux](https://www.stereolabs.com/docs/installation/linux/)

Also, install [Python API](https://www.stereolabs.com/docs/app-development/python/install/)

##  Detectron2
See [installation instructions](https://detectron2.readthedocs.io/tutorials/install.html).
``` 
no need for install detectron2, already included in here (some part have been changed from original)
#python -m pip install 'git+https://github.com/facebookresearch/detectron2.git'
# (add --user if you don't have permission)
``` 

### Changed part:
#### Add new sampler for imbalanced dataset: RepeatFactorTrainingSampler_object

To use new sampler, change following line in your train configuration file (ex. train_config.yaml):
``` 
SAMPLER_TRAIN: "RepeatFactorTrainingSampler_object" # add training sampler for imbalanced data
#default SAMPLER_TRAIN:TrainingSampler
REPEAT_THRESHOLD: 0.2791 # put the max category frequency as this threshold value (you can see the value from the output of detectron2_train.py)
# default REPEAT_THRESHOLD: 0.0
``` 

* detectron2.data.samplers.distributed_sampler: Added new sampler for imblanced dataset -> RepeatFactorTrainingSampler_object
``` 
# when calculating frequency, divide by the total objects in the dataset rather then total images
# category_freq[k] = v / num_images <= original (RepeatFactorTrainingSampler)
category_freq[k] = v / num_object <= changed (RepeatFactorTrainingSampler_object)

``` 
* detectron2.data.samplers.__init__: Add RepeatFactorTrainingSampler_object
* detectron2.data.build: Add RepeatFactorTrainingSampler_object in function '_train_loader_from_config'
``` 
from .samplers import (
    InferenceSampler,
    RandomSubsetTrainingSampler,
    RepeatFactorTrainingSampler,
    TrainingSampler,
    # Add new Sampler: RepeatFactorTrainingSampler_object,
    RepeatFactorTrainingSampler_object,
)

def _train_loader_from_config(cfg, mapper=None, *, dataset=None, sampler=None):
    #Add for new Sampler: RepeatFactorTrainingSampler_object
    elif sampler_name == "RepeatFactorTrainingSampler_object":
        repeat_factors = RepeatFactorTrainingSampler_object.repeat_factors_from_category_frequency(
            dataset, cfg.DATALOADER.REPEAT_THRESHOLD
        )
        sampler = RepeatFactorTrainingSampler_object(repeat_factors)

``` 
##  YOLOv8
See [installation instructions](https://github.com/ultralytics/ultralytics).
```
pip install ultralytics
```
##  Segment Anything
See [installation instructions](https://github.com/facebookresearch/segment-anything).

The code requires python>=3.8, as well as pytorch>=1.7 and torchvision>=0.8. Please follow the instructions [here](https://pytorch.org/get-started/locally/) to install both PyTorch and TorchVision dependencies. Installing both PyTorch and TorchVision with CUDA support is strongly recommended.

Install Segment Anything:
```
pip install git+https://github.com/facebookresearch/segment-anything.git
```
or clone the repository locally and install with
```
git clone git@github.com:facebookresearch/segment-anything.git
cd segment-anything; pip install -e .
```
The following optional dependencies are necessary for mask post-processing, saving masks in COCO format, the example notebooks, and exporting the model in ONNX format. jupyter is also required to run the example notebooks.
```
pip install opencv-python pycocotools matplotlib onnxruntime onnx
```

# Get started
First, clone the repository
```
git clone https://gitlab.epfl.ch/create-lab/robocup_at_home/object_detection.git
```

## ROS
ROS version implementation please see branch **withROS** (Object detection & Coordinate Transformation: from object to end-effector)

## Detectron2
### Training
``` 
cd Detectron2
#run script 'detectron2_train.py' in folder Detectron2: training detectron2 model
python detectron2_train.py --cfg your_config.yaml --train
``` 
#### Visualize augmented image
```
cd Detectron2
python detectron2_train.py --cfg your_config.yaml --visulize-augmented-image --visulize-dataset train1
#--visulize-dataset: dataset you want to visulize -> it should be same name in config file (TRAIN:xxx or TEST:xxx)
```
### Real-time object detection
* put the class name in your_config.yaml -> ex. train_config.yaml CLASS_NAME
``` 
cd Detectron2
#run script 'detectron2_objectdetection.py': detect object and get 3d position

#Function: zed -> detectect with zed camera
python detectron2_objectdetection.py --cfg your_config.yaml --model-weight /your/path/to/the/model/weight.pth --function zed --score-thresh 0.9

#Function: picfolder -> detectect with pictures in the provided folder
python detectron2_objectdetection.py --cfg your_config.yaml --model-weight /your/path/to/the/model/weight.pth --function picfolder --image-folder /your/path/to/image/folder

#Function: pic -> detectect with one provided picture
python detectron2_objectdetection.py --cfg your_config.yaml --model-weight /your/path/to/the/model/weight.pth --function pic --image-path /your/path/to/image

#Function: zedpic -> detectect with a picture from zed camera
python detectron2_objectdetection.py --cfg your_config.yaml --model-weight /your/path/to/the/model/weight.pth --function zedpic --score-thresh 0.9

``` 
### Evaluation
```
cd Detectron2
python detectron2_evaluation.py --cfg your_config.yaml --model-weight /yout/path/to/the/model/weight.pth
```
###  Result
![example_object_detection.gif](result/example_detectron2_object_detection.gif)
![detectron2_trained_model_result.gif](result/detectron2_trained_model_result.gif)

## YOLOv8 + Segmentation Anything (SAM)
### Training (YOLOv8)
Change the hyperparameters in your custom configuration file (ex. yolov8_custom_cfg.yaml). 
* data: path to data file -> change this to your own data file path (ex.yolo_data.yaml)
```
cd Yolov8SAM
python yolov8_train.py --cfg your_yolo_custom_cfg.yaml --pretrained-model /your/path/to/the/pretrained/model.pt --image-size InputImageSize
```
### Real-time object detection
For SAM, download the [model checkpoint](https://github.com/facebookresearch/segment-anything#model-checkpoints) first. And put in folder 'Yolov8SAM/sam_checkpoints'
Here is using "sam_vit_b_01ec64.pth"
```
cd Yolov8SAM

#Function: zed -> detectect with zed camera
python yolov8_sam.py --model-weight /yout/path/to/the/model/weight.pt 

#Function: picfolder -> detectect with pictures in the provided folder
python yolov8_sam.py --model-weight /your/path/to/the/model/weight.pt --image-folder /your/path/to/image/folder --function picfolder
```
###  Result
![example_yolov8_sam.gif](result/example_yolov8_sam.gif)

# Dataset and trained model
RoboSoft 2023: https://drive.google.com/drive/folders/1jryQSWNlDJ836oLrQ4IquYXk5eWOy-dv?usp=drive_link
* annotation: boundingbox -> [xmin,ymin,w,h]
## Visualize dataset
* annotation file should be COCO format
``` 
python dataset_visualization.py --image-path /put/your/path/to/images --annotation-path /put/your/path/to/annotation/file
``` 

## Trained model
### Detectron 2
* Trained model for Detectron2 MaskRCNN Resnet50
* Trained model for object: ['bottle','bowls','box','broccoli','carrots','cookie','cups','eggs','foodcontainers','greenbeans','meatballs','orangejuice','plates','sausage','spaghettinoodles','tray','icegems']

Add/Change the following for using in real-time object detection:

* model_best_train_output_batch4_epoch50_lr0.001_202304060035.pth: label one by one but greenchbeans bunch, also icegems bunch (surprise food)
 
  CLASS_NAME: ['bottle','bowls','box','broccoli','carrots','cookie','cups','eggs','foodcontainers','greenbeans','meatballs','orangejuice','plates','sausage','spaghettinoodles','tray','icegems']


* model_best_train_output_batch4_epoch50_lr0.001_202304050417.pth: label one by one but greenchbeans bunch, with icegems (surprise food)

  CLASS_NAME: ['bottle','bowls','box','broccoli','carrots','cookie','cups','eggs','foodcontainers','greenbeans','meatballs','orangejuice','plates','sausage','spaghettinoodles','tray','icegems']

* model_best_train_output_batch4_epoch50_lr0.001_202303310127.pth: label one by one but greenchbeans bunch, no surprise food
  
  CLASS_NAME: ['bottle','bowls','box','broccoli','carrots','cookie','cups','eggs','foodcontainers','greenbeans','meatballs','orangejuice','plates','sausage','spaghettinoodles','tray']
### YOLOv8
*in default_setting/weights: not trained properly, for test purpose